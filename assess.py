#!/usr/bin/env python3

import argparse
import logging
import multiprocessing as mp
from pathlib import Path
from typing import List, Tuple

from tqdm import tqdm

from vorti.solver import build_regex, make_guess, solve_wordle
from vorti.util import Guess, WordList, grade_word_choice, parse_wordlist


def solve_star(args):
    return args[0], solve_wordle(*args)


def average(words: List[str], wordlist: WordList):
    pool = mp.Pool()

    if words == ["all"]:
        words = [_.word for _ in wordlist]

    print(f"Getting average for all {len(words)} words with {mp.cpu_count()} threads")

    results = dict(
        tqdm(
            pool.imap_unordered(solve_star, ((_, wordlist) for _ in words)),
            desc="Solving words",
            total=len(words),
            smoothing=0.1,
        )
    )

    for word, guesses in sorted(results.items()):
        print(f"{word}: {' '.join(_[0] for _ in guesses)}")


def leaderboard(wordlist, wordfiles):
    pool = mp.Pool()

    words = []

    with open(wordfiles[0], 'r') as tfile:
        for line in tfile:
            words.append(line.strip())

    print(f"Getting average for all {len(words)} words with {mp.cpu_count()} threads")

    results = dict(
        tqdm(
            pool.imap_unordered(solve_star, ((_, wordlist) for _ in words)),
            desc="Solving words",
            total=len(words),
            smoothing=0.1,
        )
    )
    with open('vorti-0.2.csv', 'w') as of:
        for word, guesses in sorted(results.items()):
            of.write(','.join([_.guess for _ in guesses]))
            of.write("\n")


def first_guess(target, word, count) -> Tuple[str, Guess]:
    return (word, grade_word_choice(target, word), count)


def first_guess_star(args):
    return first_guess(*args)


def solve_wordle_star(args):
    return solve_wordle(*args)


def minmax(target: str, wordlist: WordList):

    pool = mp.Pool()
    print(f"Making first guesses with all words in wordlist")

    first_guesses: List[Guess] = [
        [_]
        for _ in tqdm(
            pool.imap_unordered(
                first_guess_star, ((target, _.word, len(wordlist)) for _ in wordlist)
            ),
            desc="Grading",
            total=len(wordlist),
        )
    ]

    regexes: List[Regex] = list(
        tqdm(
            pool.imap_unordered(build_regex, first_guesses),
            desc="Building regexes",
            total=len(first_guesses),
        )
    )


def optimize(target: str, wordlist: WordList):
    print(f"Finding optimal guesses for {target}")

    pool = mp.Pool()
    print(f"Making first guesses with all words in wordlist")

    first_guesses: List[Guess] = list(
        tqdm(
            pool.imap_unordered(
                first_guess_star, ((target, _.word, len(wordlist)) for _ in wordlist)
            ),
            desc="Grading",
            total=len(wordlist),
        )
    )

    print(f"Solving with first guesses")
    solutions: List[List[Guess]] = list(
        tqdm(
            pool.imap_unordered(
                solve_wordle_star,
                ((target, wordlist, [_]) for _ in first_guesses),
            ),
            desc="Solving",
            total=len(wordlist),
            smoothing=0.1,
        )
    )

    for guesses in sorted(solutions, key=len, reverse=True):
        print(f"{' '.join(f'{_[0]}:{_[2]}' for _ in guesses)}")


def _main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "action", type=str, choices=["guess", "analyze", "average", "optimize", "minmax", "leaderboard"]
    )
    parser.add_argument("wordlist", type=Path)
    parser.add_argument("word", type=str, nargs="+")

    args = parser.parse_args()

    wordlist = parse_wordlist(args.wordlist)

    logging.basicConfig(level=logging.WARNING)

    if args.action == "average":
        average(args.word, wordlist)
    elif args.action == "leaderboard":
        leaderboard(wordlist, args.word)
    elif args.action == "optimize":
        optimize(args.word[0], wordlist)
    elif args.action == "minmax":
        minmax(args.word[0], wordlist)
    elif args.action == 'guess':
        past_guesses = [Guess(_[:5],_[5:], 0) for _ in args.word]
        guesses = make_guess(wordlist, past_guesses)
        for g in guesses[-10:]:
            print(g)


if __name__ == "__main__":
    _main()
