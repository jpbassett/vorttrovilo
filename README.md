# `vorti`

A python wordle word solver.
It's not the fastest, it's not the best, but it works, kinda!

## Using vorttrovilo

Once it's installed, run `vorti -h` to see all available command line options.
`vorti` requires a word-frequency list (a word, a space, and a number) and one or more words to solve.
If the `--db` option is given, `vorti` will keep a history of seen words and past solutions.

## Getting Started

1. Clone the repository
2. Install python >=3.8
3. Install poetry with `pip install poetry`
4. Install dependencies with `pip update`
5. Launch a poetry shell (if you're not in a venv already) with `poetry shell`
6. Use the make file to build and test with `make`, `make all` or `make format lint test`