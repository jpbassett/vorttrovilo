import pytest

from vorti.util import grade_word_choice as grade


def test_all_green():
    assert "ggggg" == grade("point", "point")


def test_all_yellow():
    assert "yyyyy" == grade("abcde", "bcdea")


def test_all_grey():
    assert "....." == grade("abcde", "fghij")


def test_repeats():
    assert "....." == grade("robot", "yyyyy")
    assert ".g..." == grade("robot", "yoyyy")
    assert ".g.g." == grade("robot", "ooooo")
    assert "y.y.." == grade("robot", "oyoyy")
    assert "yg..." == grade("robot", "ooyyy")
