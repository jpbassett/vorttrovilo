from argparse import ArgumentTypeError as ATE

import pytest

from vorti import __version__
from vorti.__main__ import _parse_word


def test_version():
    assert __version__ == "0.2.2"


bad_args = [
    "",
    "a",
    "ab",
    "abc",
    "abcd",
    "abcdef",
    "1",
    "a1cde",
    "12345",
    "a!$#%",
    "!@#%^",
]


@pytest.mark.parametrize("arg", bad_args)
def test_word_parser_bad(arg):
    with pytest.raises(ATE):
        _parse_word("")


def test_word_parser():
    _parse_word("robot")
    _parse_word("pixie")
