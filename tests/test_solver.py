from vorti.solver import Score


def test_score_sum():
    score = Score()
    assert score.sum == 0

    score.commonality = 1
    score.unique_letters = 1
    assert score.sum == 2

    for idx in range(5):
        score.letters_total[idx] = 1 + idx
        score.letters_valid[idx] = 1 + idx
        score.letters_overall[idx] = 1 + idx

    assert score.sum == 2 + (1 + 2 + 3 + 4 + 5) * 3
