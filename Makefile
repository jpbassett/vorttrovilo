SOURCES := vorti assess.py
TESTS := tests
ALL := $(SOURCES) $(TESTS)

all: format lint test

format: autoflake isort black

autoflake: $(SOURCES)
	autoflake -i -r $^

isort: $(ALL) | autoflake
	isort $^

black: $(ALL) | isort
	black -q $^

lint: mypy flake8

mypy: $(ALL)
	mypy $^

flake8: $(SOURCES)
	flake8 $(SOURCES)

test: pytest

pytest: $(ALL)
	pytest -n $(shell nproc) -m 'not slow' --cov=vorti --cov-report=xml:coverage.xml --cov-report=term $(TESTS)

dist: build
build:
	poetry build

install: build
	poetry install -v
