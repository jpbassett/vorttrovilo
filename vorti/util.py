import logging
import re
from pathlib import Path
from typing import List, NamedTuple

import vorti

log = logging.getLogger(__name__)


class Count(NamedTuple):
    word: str
    wcount: int


WordList = List[Count]


class Guess(NamedTuple):
    guess: str
    result: str
    total_choices: int


def parse_wordlist(path: Path) -> WordList:
    ret: WordList = []
    with path.open("r") as infile:
        for line in (_.strip() for _ in infile):
            if not line:
                continue
            tok = line.split()
            if len(tok[0]) == 5:
                ret.append(Count(tok[0], int(tok[1])))
    return ret


def grade_word_choice(target: str, guess: str) -> str:
    """Produce a grade for a word choice

    Grading Rules:
        - If a letter is exactly right, it is GREEN
        - If a letter is in the target word, and that letter is not green, it is YELLOW
        - Otherwise, the letter is GRAY

    Args:
        target: The target word
        guess: The chosen word

    Returns:
        A 5 character string with the result:
            . is a GRAY square
            y is a YELLOW square
            g is a GREEN square

    """
    result = ["."] * 5
    matched = [False] * 5

    log.info("Validating %r == %r", target, guess)

    # Check for greens
    for idx, (t, c) in enumerate(zip(target, guess)):
        if c == t:
            result[idx] = "g"
            matched[idx] = True

    # Check for yellows
    for idx, c in enumerate(guess):
        if result[idx] == "g":
            # If this is a green character, we good
            continue
        for tidx in (_.start() for _ in re.finditer(c, target)):
            if not matched[tidx]:
                result[idx] = "y"
                matched[tidx] = True
                break

    return "".join(result)


DISCORD = {".": r"⬛", "y": r"🟨", "g": r"🟩"}


def print_guesses(
    guesses: List[Guess],
    id: str = "???",
    spoiler: bool = True,
    show_answers: bool = True,
):
    print(f"VORTI (v{vorti.__version__}) {id} {len(guesses)}/6")
    for guess, result, choices in guesses:
        rslt = ""
        for c in result:
            rslt += DISCORD[c]
        answer = ""
        if show_answers:
            if spoiler:
                answer = f" ||`{guess.upper()}` `{choices:-5d}`||"
            else:
                answer = f" `{guess.upper()}` `{choices:-5d}`"
        print(f"{rslt}{answer}")
