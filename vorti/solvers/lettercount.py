from collections import Counter
from dataclasses import dataclass
import logging
from operator import attrgetter, itemgetter
import re
from typing import List, NamedTuple, Pattern
from typing import Counter as CounterType
from vorti.solvers.solver_base import GameState, History, Solver, WordScore
from vorti.util import WordList

log = logging.getLogger(__name__)


@dataclass
class Wordstats:
    letter_pos_total: List[CounterType[str]]
    letter_pos_valid: List[CounterType[str]]
    letters_total: CounterType[str]
    total_words: int
    total_valid: int
    total_word_count: int
    total_valid_count: int

    def __init__(
        self,
        letter_pos_total: List[CounterType[str]] = None,
        letter_pos_valid: List[CounterType[str]] = None,
        letters_total: CounterType[str] = None,
        total_words: int = 0,
        total_valid: int = 0,
    ):
        self.letter_pos_total = (
            letter_pos_total
            if letter_pos_total is not None
            else [Counter() for _ in range(5)]
        )
        self.letter_pos_valid = (
            letter_pos_valid
            if letter_pos_valid is not None
            else [Counter() for _ in range(5)]
        )
        self.letters_total = letters_total if letters_total is not None else Counter()
        self.total_words = total_words
        self.total_valid = total_valid


class Score:
    def __init__(self):
        self.letters_total: List[float] = [0.0] * 5
        self.letters_valid: List[float] = [0.0] * 5
        self.letters_overall: List[float] = [0.0] * 5
        self.commonality: float = 0.0
        self.unique_letters: float = 0.0

    def __str__(self) -> str:
        return f"{self.sum:5.4f}"

    def __repr__(self) -> str:
        letters = zip(self.letters_total, self.letters_valid, self.letters_overall)
        summed = [f"{sum(_):3.2f}" for _ in letters]
        return "<Score {s:3.2f} [{l}] C[{c:3.2f}] U[{u:3.2f}]>".format(
            s=self.sum,
            l=", ".join(summed),
            c=self.commonality,
            u=self.unique_letters,
        )

    @property
    def sum(self) -> float:
        # Fancy unpacking list comprehension to avoid multiple sums
        return (
            sum(
                i
                for s in (self.letters_total, self.letters_valid, self.letters_overall)
                for i in s
            )
            + self.commonality
            + self.unique_letters
        )



class LetterCountSolver(Solver):
    def __init__(self, wordlist: WordList):
        super().__init__(wordlist)
        self.valid: WordList = wordlist
        self.state: GameState = GameState()
        self.regex: Pattern = re.compile("")
        self.stats: Wordstats = Wordstats
        self.analyzed: bool = False

    def load_guesses(self, history: List[History]):
        self.state = GameState.get_state(history)
        self.regex = self.state.get_regex()
        self.filter_wordlist(self.regex)

    def filter_wordlist(self, regex: Pattern):
        self.valid = [_ for _ in self.wordlist if regex.match(_.word)]

    def analyze_wordlist(self):
        stats = Wordstats()
        log.debug("Counting letters by position in %d total words", len(self.wordlist))
        for word, _ in self.wordlist:
            for idx, c in enumerate(word):
                stats.letter_pos_total[idx][c] += 1

        log.debug("Counting letters by position in %d valid words", len(self.valid))
        for word, _ in self.valid:
            for idx, c in enumerate(word):
                stats.letter_pos_valid[idx][c] += 1

        log.debug("Counting letters by commonality")
        for idx in range(5):
            for c, v in stats.letter_pos_total[idx].items():
                # stats.letters_total[c] += v
                stats.letters_total[c] += 0

        stats.total_words = len(self.wordlist)
        stats.total_valid = len(self.valid)
        stats.total_word_count = sum(_.wcount for _ in self.wordlist)
        stats.total_valid_count = sum(_.wcount for _ in self.valid)
        self.stats = stats
        self.analyzed = True

    def score_word(self, word: str) -> float:
        score = Score()
        if not self.analyzed:
            self.analyze_wordlist()
        for idx, c in enumerate(word.word):
            if self.state.green[idx]:
                continue
            # How common this letter is in this position for all words
            score.letters_total[idx] = (
                self.stats.letter_pos_total[idx][c] / self.stats.total_words
            )
            # How common this letter is in this position in the remaining words
            score.letters_valid[idx] = (
                self.stats.letter_pos_valid[idx][c] / self.stats.total_valid
            )
            # How common this letter is overall (0.5)
            # score.letters_total[idx] = stats.letters_total[c] / stats.total_words / 2

        score.unique_letters = len(set(word.word)) / 5.0
        if self.state.rounds > 2:
            score.commonality = (
                float(word.wcount) / self.stats.total_word_count
                + float(word.wcount) / self.stats.total_valid_count
            )

        return score.sum

    def score_all_words(self) -> List[WordScore]:
        scores = [WordScore(_, self.score_word(_)) for _ in self.valid]
        return sorted(scores, key=itemgetter(1))

    def __repr__(self) -> str:
        return f"LCSolver<WL: {len(self.valid):5d}/{len(self.wordlist):5d}, '{self.state.get_regex().pattern}'>"
