from vorti.solvers.solver_base import Solver


class EntropySolver(Solver):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def score_word(self, word: str) -> float:
        pass