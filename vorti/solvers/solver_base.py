from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import reduce
import operator
import re
from typing import NamedTuple, Pattern, Set, List

from vorti.util import WordList
import logging


class History(NamedTuple):
    guess: str
    result: str


class WordScore(NamedTuple):
    word: str
    score: float

    def __str__(self) -> str:
        return f"{self.word.word}[{self.score:7.5f}]"

    def __repr__(self) -> str:
        return str(self)


@dataclass
class GameState:
    red: Set[str]
    yellow: List[Set[str]]
    green: List[str]
    rounds: int

    def __init__(
        self,
        red: Set[str] = None,
        yellow: Set[str] = None,
        green: List[str] = None,
        rounds: int = 0,
    ):
        self.red = red if red is not None else set()
        self.yellow = yellow if yellow is not None else [set() for _ in range(5)]
        self.yellows = reduce(operator.or_, self.yellow)
        self.green = green if green is not None else [""] * 5
        self.rounds = rounds

    @classmethod
    def get_state(cls, history: List[History]) -> "GameState":
        state: GameState = cls()
        for guess, rslt in history:
            for idx, c in enumerate(guess):
                if rslt[idx] == "." and c not in state.yellows and c not in state.green:
                    state.red.add(c)
                elif rslt[idx] == "y":
                    state.yellow[idx].add(c)
                    state.yellows.add(c)
                elif rslt[idx] == "g":
                    state.green[idx] = c
        state.rounds = len(history)
        return state

    def get_regex(self) -> Pattern:
        log = logging.getLogger(__name__)

        rpat = f"(?!.*[{''.join(sorted(self.red))}])" if self.red else ""
        ypat = "".join([f"(?=.*{_})" for _ in self.yellows - set(self.green)])
        gpat = ""
        for idx, c in enumerate(self.green):
            if c:
                gpat += c
            elif self.yellow[idx]:
                gpat += f"[^{''.join(self.yellow[idx])}]"
            else:
                gpat += "."

        patstr = f"^{rpat}{ypat}{gpat}"
        pattern = re.compile(patstr)
        return pattern


class Solver(ABC):
    def __init__(self, wordlist: WordList = None):
        self.wordlist = wordlist if wordlist is not None else []
        self.state = GameState()

    @abstractmethod
    def score_word(self, word: str) -> float:
        pass

    @abstractmethod
    def score_all_words(self) -> List[WordScore]:
        pass

    @abstractmethod
    def load_guesses(self, history: List[History]):
        pass