import logging
import operator
import re
from collections import Counter
from dataclasses import dataclass
from functools import reduce
from typing import Counter as CounterType
from typing import List, Set, Tuple

from .util import Count, Guess, WordList, grade_word_choice

log = logging.getLogger(__name__)


@dataclass
class Wordstats:
    letter_pos_total: List[CounterType[str]]
    letter_pos_valid: List[CounterType[str]]
    letters_total: CounterType[str]
    total_words: int
    total_valid: int

    def __init__(
        self,
        letter_pos_total: List[CounterType[str]] = None,
        letter_pos_valid: List[CounterType[str]] = None,
        letters_total: CounterType[str] = None,
        total_words: int = 0,
        total_valid: int = 0,
    ):
        self.letter_pos_total = (
            letter_pos_total
            if letter_pos_total is not None
            else [Counter() for _ in range(5)]
        )
        self.letter_pos_valid = (
            letter_pos_valid
            if letter_pos_valid is not None
            else [Counter() for _ in range(5)]
        )
        self.letters_total = letters_total if letters_total is not None else Counter()
        self.total_words = total_words
        self.total_valid = total_valid


class Score:
    def __init__(self):
        self.letters_total: List[float] = [0.0] * 5
        self.letters_valid: List[float] = [0.0] * 5
        self.letters_overall: List[float] = [0.0] * 5
        self.commonality: float = 0.0
        self.unique_letters: float = 0.0

    def __str__(self) -> str:
        return f"{self.sum:5.4f}"

    def __repr__(self) -> str:
        letters = zip(self.letters_total, self.letters_valid, self.letters_overall)
        summed = [f"{sum(_):3.2f}" for _ in letters]
        return "<Score {s:3.2f} [{l}] C[{c:3.2f}] U[{u:3.2f}]>".format(
            s=self.sum,
            l=", ".join(summed),
            c=self.commonality,
            u=self.unique_letters,
        )

    @property
    def sum(self) -> float:
        # Fancy unpacking list comprehension to avoid multiple sums
        return (
            sum(
                i
                for s in (self.letters_total, self.letters_valid, self.letters_overall)
                for i in s
            )
            + self.commonality
            + self.unique_letters
        )


def score_word(
    word: Count,
    stats: Wordstats,
    green: List[str],
    total_count: int,
    total_valid: int,
    rounds: int,
) -> Score:
    score = Score()

    # Give 0-1 points for how common each non-green character is overall
    for idx, c in enumerate(word.word):
        if green[idx]:
            continue
        # How common this letter is in this position for all words
        score.letters_total[idx] = stats.letter_pos_total[idx][c] / stats.total_words
        # How common this letter is in this position in the remaining words
        score.letters_valid[idx] = stats.letter_pos_valid[idx][c] / stats.total_valid
        # How common this letter is overall (0.5)
        # score.letters_total[idx] = stats.letters_total[c] / stats.total_words / 2

    score.unique_letters = len(set(word.word)) / 5.0
    if rounds > 2:
        score.commonality = (
            float(word.wcount) / total_count + float(word.wcount) / total_valid
        )

    return score


def analyze_wordlist(wordlist: WordList, valid: WordList) -> Wordstats:
    stats = Wordstats()
    log.debug("Counting letters by position in %d total words", len(wordlist))
    for word, _ in wordlist:
        for idx, c in enumerate(word):
            stats.letter_pos_total[idx][c] += 1

    log.debug("Counting letters by position in %d valid words", len(valid))
    for word, _ in valid:
        for idx, c in enumerate(word):
            stats.letter_pos_valid[idx][c] += 1

    log.debug("Counting letters by commonality")
    for idx in range(5):
        for c, v in stats.letter_pos_total[idx].items():
            # stats.letters_total[c] += v
            stats.letters_total[c] += 0

    stats.total_words = len(wordlist)
    stats.total_valid = len(valid)
    return stats


def build_regex(guesses: List[Guess]) -> Tuple[re.Pattern, List[str]]:
    # Letters we know are red
    rletters: Set[str] = set()
    rletterset: List[Set[str]] = [set() for _ in range(5)]
    # Yellow letters by position (known to not be there)
    yletters: List[Set[str]] = [set() for _ in range(5)]
    # Any green letters
    gletters: List[str] = ["" for _ in range(5)]

    if log.isEnabledFor(logging.DEBUG):
        for round, (guess, result, choices) in enumerate(guesses):
            log.debug("  Round %d/6 %s %s %d", round + 1, guess, result, choices)

    # Build wordlist filters
    for guess, rslt, _ in guesses:
        for idx, c in enumerate(guess):
            if rslt[idx] == ".":
                rletters.add(c)
                rletterset[idx].add(c)
            elif rslt[idx] == "y":
                yletters[idx].add(c)
            elif not gletters[idx] and rslt[idx] == "g":
                gletters[idx] = c
            else:
                pass

    # unmark red letters that are green or yellow
    yset = reduce(operator.or_, yletters)
    rletters = rletters - yset - set(gletters)
    if log.isEnabledFor(logging.DEBUG):
        log.debug("Red letters:    %r", rletters)
        log.debug("Red letter set: %r", rletterset)
        log.debug("Yellow letters: %r", yletters)
        log.debug("Green letters:  %r", gletters)

    rset = "".join(rletters)
    gpattern = ""
    gset = set(gletters)
    for idx in range(5):
        if gletters[idx]:
            gpattern += gletters[idx]
        elif yletters[idx] or rletterset[idx]:
            gpattern += f"[^{''.join(yletters[idx] | rletterset[idx])}]"
        else:
            gpattern += "."

    rpat = f"(?!.*[{rset}])" if rset else ""
    ypat = "".join([f"(?=.*{_})" for _ in yset - gset])
    patstr = f"^{rpat}{ypat}{gpattern}$"
    log.debug("Compiling pattern: %r", patstr)
    pattern = re.compile(patstr)

    return pattern, gletters


def make_guess(wordlist: WordList, guesses: List[Guess]) -> List[Tuple[Count, Score]]:

    gletters: List[str] = [""] * 5
    known_letters = Counter()
    words : List[str] = []
    if guesses:
        pattern, gletters = build_regex(guesses)

        # Filter wordlist
        # Remove words with red letters
        words: List[Count] = [_ for _ in wordlist if pattern.match(_.word)]
        known_letters = Counter(guesses[-1].guess[_] for _ in range(5) if guesses[-1].result[_] != '.')
    else:
        words = wordlist

    # Addional hard mode filter (make sure all known letters are accounted for)
    if known_letters:
        passed = []
        log.debug("Making sure all %d words have %r letters", len(words), known_letters)
        for word in words:
            wc = Counter(word.word)
            wc.subtract(known_letters)
            if not any(_ < 0 for _ in wc.values()):
                passed.append(word)
        words = passed
        log.debug("%d words remain", len(words))


    total_in_wordlist = sum(_.wcount for _ in wordlist)
    total_in_words = sum(_.wcount for _ in words)

    metrics = analyze_wordlist(wordlist, words)

    wordscores: List[Tuple[Count, Score]] = [
        (
            _,
            score_word(
                _, metrics, gletters, total_in_wordlist, total_in_words, len(guesses)
            ),
        )
        for _ in words
    ]

    wordscores.sort(key=lambda w: w[1].sum)
    return wordscores


def solve_wordle(
    target: str, wordlist: WordList, known_guesses: List[Guess] = None
) -> List[Guess]:
    past_guesses: List[Guess] = known_guesses if known_guesses is not None else []
    while True:
        log.info("%s: Round %d", target, len(past_guesses) + 1)
        guesses = make_guess(wordlist, past_guesses)
        if log.isEnabledFor(logging.DEBUG):
            log.debug("Got %d guesses", len(guesses))
            for word, score in guesses[-25:]:
                log.debug("  %s %r %d", word.word, score, word.wcount)
        if not guesses:
            log.debug("Struck out")
            break
        guess = guesses[-1]
        result = grade_word_choice(target, guess[0].word)
        log.info(
            "%s: Round %d : %r => %r", target, len(past_guesses) + 1, guess, result
        )
        past_guesses.append(Guess(guess[0][0], result, len(guesses)))
        if result == "ggggg":
            log.debug("Got it!")
            break
    return past_guesses
