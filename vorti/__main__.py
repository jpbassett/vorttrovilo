#!/usr/bin/env python3

import argparse
import json
import logging
from pathlib import Path

from vorti.solver import solve_wordle

from .util import WordList, parse_wordlist, print_guesses

log = logging.getLogger(__name__)


def load_db(path):
    data = {"words": [], "solvers": {}}
    try:
        with path.open("r") as infile:
            data.update(json.load(infile))
    except json.JSONDecodeError as ex:
        log.error("Failed to load solver DB: %r", ex)
    return data


def _parse_wordlist(arg: str) -> WordList:
    path = Path(arg)
    if not path.exists():
        raise argparse.ArgumentTypeError("Path does not exist")
    if not path.is_file():
        raise argparse.ArgumentTypeError("Path is not a file")
    return parse_wordlist(path)


def _parse_word(arg: str) -> str:
    arg = arg.strip().lower()
    if not arg.isalpha():
        raise argparse.ArgumentTypeError("Words must be all letters")
    if len(arg) != 5:
        raise argparse.ArgumentTypeError("Words must be exactly 5 letters")
    return arg


def _do_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Solves wordle puzzles")

    parser.add_argument(
        "-l",
        "--log-level",
        help="Log level",
        default="warning",
        choices=["debug", "info", "warning", "error"],
    )
    parser.add_argument(
        "wordlist", metavar="wordlist.txt", type=_parse_wordlist, help="WOrdlist to use"
    )
    parser.add_argument(
        "target", nargs="+", type=_parse_word, help="Word to try and solve"
    )

    parser.add_argument(
        "-w",
        "--word-limit",
        type=int,
        help="Maximum number of words to load",
        default=15000,
    )

    parser.add_argument(
        "--no-answers",
        dest="show_answers",
        action="store_false",
        help="If set, don't show answers",
    )

    parser.add_argument("--db", help="Solver history database", type=Path)

    args = parser.parse_args()

    return args


def _main():
    args = _do_args()

    logging.basicConfig(
        level=getattr(logging, args.log_level.upper()),
        format="%(asctime)s [%(levelname)-7s] %(name)s %(message)s",
    )

    dbdata = {}
    if args.db:
        log.info("Loading history")
        dbdata.update(load_db(args.db))

    log.info("Loaded %d words", len(args.wordlist))

    for word in args.target:
        log.info("Trying to solve %r", word)
        guesses = solve_wordle(word, args.wordlist)
        print_guesses(guesses, show_answers=args.show_answers)

    if args.db:
        log.info("Writing out history")
        with args.db.open("w") as outfile:
            json.dump(dbdata, outfile)


if __name__ == "__main__":
    _main()
